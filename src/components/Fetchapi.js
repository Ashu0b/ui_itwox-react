import React from 'react'
import { useState } from 'react'

export const Fetchapi = () => {
    const [data,setData]=useState([])
  const apiGet=()=>{
    fetch('https://jsonplaceholder.typicode.com/posts')
    .then((response)=>response.json())
    .then((json)=>{
        console.log(json)
        setData(json)

    })
  }
  return (
    <div>
        My api <br>
        <buton onClick={apiGet}> fetch API</buton>
        </br>
        <pre>{JSON.stringify(data,null,2)}</pre>
    </div>
  )
}
export default Fetchapi