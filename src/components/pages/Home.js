import { Grid } from "@mui/material";

const Home = () => {
  return <>
    <Grid container justifyContent='center'>
      <Grid item sm={10}>
        <h1>Home Page</h1>
        <hr />
        <p>you need to login/register to see the Dashboard</p>
      </Grid>
    </Grid>
  </>;
};

export default Home;
